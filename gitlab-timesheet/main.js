/* eslint-disable prefer-destructuring,no-console,no-await-in-loop,no-plusplus */
const axios = require('axios');
const moment = require('moment');
const fs = require('fs');
const asyncPool = require('tiny-async-pool');
const parseDuration = require('./parseduration');

let token;
try {
  token = require('./token.json');
} catch (e) {
  console.error('Error: token.json not found');
  console.error('Create a personal access token (API) here: https://gitlab.com/profile/personal_access_tokens');
  console.error('Copy token.sample.json to token.json and replace code with your own API access token from above');
  console.error('$ cp token.sample.json token.json');
  process.exit(1);
}

const fromFile = null;
/* const fromFile = 'out/2019-03-02.json'; */

// process.argv.slice(2)[0]
const startOfMonth = moment()
  .startOf('month')
  .format('YYYY-MM-DD');
const endOfMonth = moment()
  .endOf('month')
  .format('YYYY-MM-DD');
let startDate = startOfMonth;
let endDate = endOfMonth;

if (process.argv.length === 3) {
  startDate = process.argv.slice(2)[0];
  endDate = moment().format('YYYY-MM-DD');
} else if (process.argv.length === 4) {
  startDate = process.argv.slice(2)[0];
  endDate = process.argv.slice(2)[1];
} else if (process.argv.lentgh > 4) {
  console.error('too many arguments');
  process.exit(1);
}

console.log(`start date: ${startDate}`);
console.log(`end date: ${endDate}`);

const config = {
  group: 'our-sci',
  token: token.code,
  base: 'https://gitlab.com/api/v4',
  projectsList: [
    'research-farm-roadmap',
    'documentation',
    'vscode',
    'npm-libs',
    'dashboards',
    'webserver',
    'measurement-scripts',
    'android',
    'reflectance-spec-firmware',
    'mockups',
  ],
  threads: 20,
  startDate,
  endDate,
  notesDir: 'notes',
};

if (!config.token) {
  console.error('pass personal access token as argument');
  console.log('https://gitlab.com/profile/personal_access_tokens');
  process.exit(1);
}

const opts = {
  headers: {
    'PRIVATE-TOKEN': config.token,
  },
};

const re = /^(added|subtracted) (.*?) of time spent at (.*?)$/;

async function fetchIssue(project) {
  const responses = [];
  let page = 1;
  for (;;) {
    const response = await axios.get(`${config.base}/projects/${project.id}/issues?per_page=100&page=${page}`, {
      opts,
    });

    responses.push(response);
    console.log(`  "${project.path}" [${project.id}]: page ${page}`);
    if (!response.headers['x-next-page']) {
      break;
    }
    page++;
  }
  return responses;
}

function fetchNotes(project, issueId) {
  return axios.get(
    `${config.base}/projects/${project}/issues/${issueId}/notes`,
    Object.assign({}, opts, {
      additional: {
        project,
        issueId,
      },
    })
  );
}

function processNotes(notes) {
  if (!fromFile) {
    const { notesDir } = config;
    if (!fs.existsSync(notesDir)) {
      fs.mkdirSync(notesDir);
    }
    fs.writeFileSync(`${notesDir}/${moment().format('YYYY-MM-DD')}.json`, JSON.stringify(notes, null, 4));
  }

  const reportName = 'report.csv';
  if (fs.existsSync(reportName)) {
    fs.unlinkSync(reportName);
  }

  const report = fs.openSync(reportName, 'a');
  fs.writeSync(report, 'autor\tcreated\tspent on\thours\traw\n');

  const f = notes.filter((d) => d.system && d.body.match(re));

  const membersByDate = {};
  const parsed = f
    .map((item) => {
      const r = item.body.match(re);

      if (r === null) {
        console.error('cannot extract time');
        console.error(item.body);
        return null;
      }

      const created = moment(r[3]);
      created.add(1, 'h');

      if (!created.isBetween(moment(config.startDate), moment(config.endDate))) {
        return null;
      }

      const d = parseDuration(r[2]);
      const duration = r[1] === 'added' ? d : -d;
      if (!(item.author.name in membersByDate)) {
        membersByDate[item.author.name] = {};
      }

      fs.writeSync(
        report,
        `${item.author.name}\t${item.created}\t${created.format('YYYY-MM-DD')}\t${duration / (1000 * 60 * 60)}\t${
          r[2]
        }\n`
      );

      const date = created.format('YYYY-MM-DD');

      if (!(date in membersByDate[item.author.name])) {
        membersByDate[item.author.name][date] = duration / (1000 * 60 * 60);
      } else {
        membersByDate[item.author.name][date] += duration / (1000 * 60 * 60);
      }

      return {
        user: item.author.name,
        duration,
        issue: item.issue,
        project: item.project,
      };
    })
    .filter((d) => d !== null);

  fs.closeSync(report);
  const summary = {};
  parsed.forEach((p) => {
    if (!summary[p.user]) {
      summary[p.user] = 0;
    }
    summary[p.user] += p.duration;
  });

  console.log(`start date: ${moment(config.startDate)}`);
  console.log(`end date:   ${moment(config.endDate)}`);

  const users = [];
  Object.keys(summary).forEach((user) => {
    users.push(user);
    console.log(`${user}: hours ${moment.duration(summary[user]).as('hours')}`);
  });

  console.log('');

  Object.keys(membersByDate).forEach((k) => {
    const total = moment
      .duration(summary[k])
      .as('hours')
      .toFixed(2);
    const time = membersByDate[k];
    console.log(`${k}: ${total} hours`);

    const dates = Object.keys(time).sort();
    dates.forEach((date) => {
      const hours = time[date].toFixed(2).padStart(10, ' ');
      console.log(`  ${date}\t${hours}`);
    });
    console.log('  ------------------------');
    console.log(`  Total\t\t${total.padStart(10, ' ')}`);
    console.log('');
  });
  /*   users.forEach((u) => {
    const filtered = parsed.filter((p) => p.user === u);
    const byIssue = {};
    filtered.forEach((f) => {
      const key = `${f.project} > ${f.issue}`;
      if (byIssue[key]) {
        byIssue[key] = {};
        byIssue[key].duration = f.duration;
        byIssue.project = f.project;
        byIssue.title = f.issue;
      } else {
        byIssue[key].duration += f.duration;
      }
    });
  }); */
}

(async function() {
  if (fromFile) {
    processNotes(JSON.parse(fs.readFileSync(fromFile)));
    return;
  }
  const { base, group, projectsList, threads } = config;

  try {
    console.log('Fetching all projects...');
    const gitlabProjects = await axios.get(`${base}/groups/${group}/projects`, opts);

    const projects = gitlabProjects.data.filter((p) => {
      if (projectsList.includes(p.path)) {
        console.log(`  "${p.path}" [${p.id}]`);
        return true;
      }

      console.warn(`  [!] "${p.path}" [${p.id}] ignored (not in config)`);
      return false;
    });

    console.log('Fetching all issues in all projects...');

    const issuesResponses = (await asyncPool(threads, projects, (p) => fetchIssue(p))).flatMap((r) => r);

    const issues = issuesResponses.flatMap((response) => {
      return response.data.flatMap((issue) => {
        return {
          project_id: issue.project_id,
          id: issue.iid,
        };
      });
    });

    console.log('Fetching all notes in all issues...');

    const noteResponses = await asyncPool(threads, issues, (i) => fetchNotes(i.project_id, i.id));

    const notes = noteResponses.flatMap((noteResponse) => {
      return noteResponse.data.flatMap((n) => {
        return {
          created: n.created_at,
          body: n.body,
          system: n.system,
          author: n.author,
          issue: noteResponse.config.additional.issueId,
          project: noteResponse.config.additional.project,
        };
      });
    });

    processNotes(notes);
  } catch (error) {
    console.error(error);
  }
})();
