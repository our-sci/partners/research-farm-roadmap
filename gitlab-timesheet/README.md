# Time Tracking Tool

This tools pulls time spent across issues and projects of the our-sci organization.

To access the API create a personal access token over here <https://gitlab.com/profile/personal_access_tokens>

create a file `token.json` with contents:
```json
{
  "code": "<token goes here>"
}
```

```bash
yarn install

# edit start / end date and projects in main.js
node main.js
```

The tool will accumulate hours spent between specified dates according to the projects selected in `main.js`.